//
//  InstargamLoginViewController.h
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramView.h"

@protocol InstagramAuthDelegate <NSObject>

- (void)didAuthWithParams:(NSDictionary *)params;

@end


@interface InstargamLoginViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic) InstagramView *view;
@property (nonatomic, weak) id<InstagramAuthDelegate> authDelegate;

@end

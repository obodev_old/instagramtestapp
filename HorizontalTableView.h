//
//  HorizontalTableView.h
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HorizontalTableViewCell.h"

@class HorizontalTableView;

@protocol HorizontalTableViewDelegate <NSObject, UIScrollViewDelegate>

@required
- (NSInteger)numberOfCells;
- (NSInteger)widthForCellAtIndexPath:(NSInteger)index;
- (HorizontalTableViewCell *)tableView:(HorizontalTableView *)tableView cellForRowAtIndexPath:(NSInteger)indexPath;

@end


@interface HorizontalTableView : UIScrollView

@property (nonatomic, weak) id <HorizontalTableViewDelegate> tableViewDelegate;

- (void)registerClass:(Class)cellClass forCellReuseIdentifier:(NSString *)identifier;
- (HorizontalTableViewCell *)dequeueReusableCellWithIdentifier:(NSString *)identifier;
- (HorizontalTableViewCell *)cellForRowAtIndexPath:(NSInteger)indexPath;

- (NSMutableArray *)indexPathsForVisibleRows;
- (void)prepareTableView;
- (void)didScrollToTheLeft;
- (void)didScrollToTheRight;

- (void)reloadData;

@end

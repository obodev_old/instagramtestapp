//
//  InstagramView.m
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "InstagramView.h"

@implementation InstagramView

- (instancetype)init {
    self = [super init];
    if (self) {
        _webview = [UIWebView new];
        [self addSubview:_webview];
    }
    
    return self;
}

- (void)layoutSubviews {
    _webview.frame = self.frame;
}

@end

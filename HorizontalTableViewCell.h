//
//  HorizontalTableViewCell.h
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HorizontalTableViewCell : UIView

@property (nonatomic) NSString *reuseIdentifier;
@property (nonatomic, assign) NSInteger indexInTableView;

@property (nonatomic) UILabel *defaultLabel;

- (void)resetAllData;

@end

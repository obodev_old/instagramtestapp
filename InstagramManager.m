//
//  InstagramManager.m
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "InstagramManager.h"
#import "APIHelper.h"
#import "MediaData.h"


@implementation InstagramManager {
    void(^completionHandler)(NSString *result);
    UINavigationController *rootNavController;
}

- (instancetype)init {
    self = [super init];
    if (self ) {
        rootNavController = (UINavigationController *)((UIWindow *)[UIApplication sharedApplication].windows[0]).rootViewController;
    }
    
    return self;
}

+ (InstagramManager *)sharedInstance {
    static dispatch_once_t once;
    static InstagramManager *sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [InstagramManager new];
    });
    
    return sharedInstance;
}

#pragma mark Login

- (void)loginWithCompletion:(void(^)(NSString *result))completion {
    InstargamLoginViewController *instagramVC = [InstargamLoginViewController new];
    instagramVC.authDelegate = self;
    
    completionHandler = completion;
    [rootNavController presentViewController:instagramVC animated:YES completion:nil];
}

- (void)didAuthWithParams:(NSDictionary *)params {
    [rootNavController dismissViewControllerAnimated:YES completion:nil];

    if (params) {
        _uniqueToken = params[@"access_token"];
        _userID      = params[@"user"][@"id"];
        
        if (completionHandler) {
            completionHandler(@"OK");
        }
        return;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Failed to request token."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
    if (completionHandler) {
        completionHandler(@"Error");
    }
}

#pragma mark Loading media

- (void)loadAllMediaWithCompletion:(void(^)(NSArray *result))completion {
    [[APIHelper sharedHelper] loadRecentMediaWithCompletion:^(NSArray *result){
        _mediaData = [self parseReceivedInfo:result];
        completion(_mediaData);
    }];
}

- (NSMutableArray *)parseReceivedInfo:(NSArray *)receivedInfo {
    NSMutableArray *parsedData = [NSMutableArray new];
    
    for (NSDictionary *dict in receivedInfo) {
        MediaData *mediaData     = [MediaData new];
        mediaData.mediaType      = dict[@"type"];
        mediaData.totalComments  = [dict[@"comments"][@"count"] integerValue];
        mediaData.mediaURLString = dict[[NSString stringWithFormat:@"%@s", mediaData.mediaType]][@"low_resolution"][@"url"];
        
        [parsedData addObject:mediaData];
    }
    
    [self sortMediaData:parsedData];
    
    return parsedData;
}

- (void)sortMediaData:(NSMutableArray *)data {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"totalComments" ascending:NO];
    [data sortUsingDescriptors:@[sortDescriptor]];
}

@end


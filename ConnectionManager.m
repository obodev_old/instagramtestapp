//
//  ConnectionManager.m
//  Limeet
//
//  Created by Fantom on 7/26/15.
//  Copyright (c) 2015 Obodev. All rights reserved.
//

#import "ConnectionManager.h"

@implementation ConnectionManager {
    NSMutableData *responseData;
    void(^completion)(NSData *result);
}

- (void)loadDataFromURL:(NSURL *)url withCompletion:(void(^)(NSData *result))completionHandler {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:20.0];
    
    //Assign completion handler
    completion = completionHandler;
    
    // Create url connection and fire request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [NSMutableData new];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    completion(responseData);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
    
    completion(nil);
    
    NSLog(@">> ::Connection was failed with error:%@", error);
}

//- (void)dealloc {
//    NSLog(@"Connection manager was deallocated");
//}

@end

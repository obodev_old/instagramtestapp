//
//  ImageData.h
//  TestApp
//
//  Created by Fantom on 6/30/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MediaData : NSObject

@property (nonatomic)         NSString *mediaType;
@property (nonatomic)         NSData   *mediaFile;
@property (nonatomic)         NSString *mediaURLString;
//@property (nonatomic)         NSString *localPath;
@property (nonatomic)         NSString *fileName;
@property (nonatomic, assign) NSInteger totalComments;

@end

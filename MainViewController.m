//
//  MainViewController.m
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "MainViewController.h"
#import "VideoCell.h"
#import "ImageCell.h"

#import "MediaData.h"
#import "MediaLoader.h"
#import "InstagramManager.h"

#import "MBProgressHUD.h"

@implementation MainViewController {
    NSMutableArray *allMedia;
    NSMutableDictionary *allLoaders;
    UIView *blackScreen;
    
    MBProgressHUD *progressView;
}

static NSString * const imageCellReuseID = @"imageCell";
static NSString * const videoCellReuseID = @"videoCell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    allLoaders = [NSMutableDictionary new];
    
    self.horizontalTableView.tableViewDelegate = self;
    [self.horizontalTableView registerClass:[ImageCell class] forCellReuseIdentifier:imageCellReuseID];
    [self.horizontalTableView registerClass:[VideoCell class] forCellReuseIdentifier:videoCellReuseID];
    
    [[InstagramManager sharedInstance] loadAllMediaWithCompletion:^(NSArray *result){
        allMedia = [result copy];
        [self.horizontalTableView reloadData];
        [progressView hide:YES];
    }];
    
    [self addProgressView];
}

- (void)addProgressView {
    progressView               = [MBProgressHUD new];
    progressView.dimBackground = YES;
    progressView.frame         = self.view.bounds;

    [self.view addSubview:progressView];
    [progressView show:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    for (MediaLoader *loader in allLoaders) {
        [loader cancelDownload];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfCells {
    return allMedia.count;
}

- (NSInteger)widthForCellAtIndexPath:(NSInteger)index {
    return [UIScreen mainScreen].bounds.size.width;
}

- (HorizontalTableViewCell *)tableView:(HorizontalTableView *)tableView cellForRowAtIndexPath:(NSInteger)indexPath {
    HorizontalTableViewCell *cell;
    MediaData *mediaData = [allMedia objectAtIndex:indexPath];
    
    if ([mediaData.mediaType isEqualToString:@"image"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:imageCellReuseID];
        if (!mediaData.mediaFile) {
            // We will load only visible images on screen, while user is not dragging collection view
            if (self.horizontalTableView.dragging == NO && self.horizontalTableView.decelerating == NO) {
                [self startMediaDownload:mediaData forIndexPath:indexPath];
            }
            // if a download is deferred or in progress, return a placeholder image
            ((ImageCell *)cell).imgView.image = [UIImage imageNamed:@"placeholder"];
            ((ImageCell *)cell).commentsCounterLabel.text = [NSString stringWithFormat:@"total comments:%lu", mediaData.totalComments];
        } else {
            ((ImageCell *)cell).imgView.image = [UIImage imageWithData:mediaData.mediaFile];
            ((ImageCell *)cell).commentsCounterLabel.text = [NSString stringWithFormat:@"total comments:%lu", mediaData.totalComments];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:videoCellReuseID];
        if (!mediaData.mediaFile) {
            // We will load only visible images on screen, while user is not dragging collection view
            if (self.horizontalTableView.dragging == NO && self.horizontalTableView.decelerating == NO) {
                [self startMediaDownload:mediaData forIndexPath:indexPath];
            }
            
            ((VideoCell *)cell).commentsCounterLabel.text = [NSString stringWithFormat:@"total comments:%lu", mediaData.totalComments];
        } else {
            ((VideoCell *)cell).videoName = mediaData.fileName;
            [((VideoCell *)cell) prepareToPlay];
            ((VideoCell *)cell).commentsCounterLabel.text = [NSString stringWithFormat:@"total comments:%lu", mediaData.totalComments];
        }
    }
    
    return cell;
}

#pragma mark Load pictures

- (void)startMediaDownload:(MediaData *)mediaData
              forIndexPath:(NSInteger)indexPath {
    
    //Get media loader in dict by indexPath key
    MediaLoader *mediaLoader = [allLoaders objectForKey:[NSString stringWithFormat:@"%lu", indexPath]];
    
    //If there is no loader, we'r going to create one
    if (mediaLoader == nil) {
        mediaLoader = [MediaLoader new];
        mediaLoader.mediaData = mediaData;
        
        [mediaLoader setCompletionHandler:^{
            if ([mediaData.mediaType isEqualToString:@"image"]) {
                ImageCell *imgCell = (ImageCell *)[self.horizontalTableView cellForRowAtIndexPath:indexPath];
                imgCell.imgView.image = [UIImage imageWithData:mediaData.mediaFile];
            } else {
                VideoCell *videoCell = (VideoCell *)[self.horizontalTableView cellForRowAtIndexPath:indexPath];
                videoCell.videoName = mediaData.fileName;
                [videoCell prepareToPlay];
            }
          
            [allLoaders removeObjectForKey:[NSString stringWithFormat:@"%lu", indexPath]];
        }];
        
        allLoaders[[NSString stringWithFormat:@"%lu", indexPath]] = mediaLoader;
        [mediaLoader startDownload];
    }
    
}

- (void)loadImagesForOnScreenRows {
    //First we are getting all visible table view cell's rows on screen
    NSArray *visibleSectionsOnTableView = [self.horizontalTableView indexPathsForVisibleRows];

        //Here, we are getting MediaData for each visible table view cell in our array (allMedia),
        //and checking if MediaData has already an media content. If hasn't, we begin loading it's content.
        for (NSNumber *imgIndexPath in visibleSectionsOnTableView) {
            
            MediaData *targetMediaData = allMedia[[imgIndexPath intValue]];
            if (!targetMediaData.mediaFile) {
                [self startMediaDownload:targetMediaData forIndexPath:[imgIndexPath intValue]];
            }
        }
}

#pragma mark Scroll View Delegate methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self loadImagesForOnScreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self loadImagesForOnScreenRows];
}

@end

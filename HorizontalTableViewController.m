//
//  HorizontalTableViewController.m
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "HorizontalTableViewController.h"

@implementation HorizontalTableViewController  {
    CGFloat lastContentOffset;
}


- (void)loadView {
    self.horizontalTableView = [HorizontalTableView new];
    self.view = self.horizontalTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lastContentOffset = 0;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.horizontalTableView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.horizontalTableView  prepareTableView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (lastContentOffset > scrollView.contentOffset.x) {
        [self.horizontalTableView  didScrollToTheRight];
    } else if (lastContentOffset < scrollView.contentOffset.x) {
        [self.horizontalTableView  didScrollToTheLeft];
    }
    
    lastContentOffset = scrollView.contentOffset.x;
}

@end

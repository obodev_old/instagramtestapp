//
//  ImageLoader.m
//  TestApp
//
//  Created by Fantom on 6/30/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "MediaLoader.h"
#import "MediaData.h"


@implementation MediaLoader {
    NSMutableData *activeDownload;
    NSURLConnection *imageConnection;
}

- (void)startDownload {
    activeDownload = [NSMutableData data];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_mediaData.mediaURLString]];
    
    // alloc+init and start an NSURLConnection; release on completion/failure
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    imageConnection = connection;
}

- (void)cancelDownload {
    [imageConnection cancel];
    imageConnection = nil;
    activeDownload = nil;
}


#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [activeDownload appendData:data];  
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // Clear the activeDownload property to allow later attempts
    activeDownload = nil;
    
    // Release the connection now that it's finished
    imageConnection = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (!activeDownload) {
        return;
    }
    
    _mediaData.mediaFile = [activeDownload copy];
    if ([_mediaData.mediaType isEqualToString:@"video"]) {
        //gettigng name of file
        NSURL *tempUrl    = [NSURL URLWithString:_mediaData.mediaURLString];
        NSString *newPath = [[tempUrl URLByDeletingPathExtension] absoluteString];
        NSArray *parts    = [newPath componentsSeparatedByString:@"/"] ;
        _mediaData.fileName = [parts lastObject];
        
        //copying it to local storage
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *appFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", _mediaData.fileName]];
        [_mediaData.mediaFile writeToFile:appFile atomically:YES];
    }
    
    activeDownload = nil;
    
    // Release the connection now that it's finished
    imageConnection = nil;
    
    // call our delegate and tell it that our image is ready for display
    if (_completionHandler)
    {
        _completionHandler();
    }
}

@end
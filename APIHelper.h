//
//  APIHelper.h
//  Limeet
//
//  Created by Fantom on 7/14/15.
//  Copyright (c) 2015 Obodev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface APIHelper : NSObject 

+ (APIHelper *)sharedHelper;

- (BOOL)connectedToNetwork;

- (void)loadRecentMediaWithCompletion:(void(^)(NSArray *))completion;

@end

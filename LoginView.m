//
//  LoginView.m
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "LoginView.h"

@implementation LoginView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        
        _loginButton = [UIButton new];
        _loginButton.contentMode = UIViewContentModeScaleAspectFit;
        [_loginButton setBackgroundImage:[UIImage imageNamed:@"instagram_signin"] forState:UIControlStateNormal];
        [self addSubview:_loginButton];
    }
    
    return self;
}


- (void)layoutSubviews {
    static const int buttonWidth = 200;
    static const int buttonHeight = 30;

    
    _loginButton.frame = CGRectMake(CGRectGetMidX(self.frame) - buttonWidth/2,
                                    CGRectGetMaxY(self.frame) - buttonHeight - 100,
                                    buttonWidth,
                                    buttonHeight);
}

@end

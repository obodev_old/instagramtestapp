//
//  InstargamLoginViewController.m
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "InstargamLoginViewController.h"
#import "NSDictionary+UrlEncoding.h"

@implementation InstargamLoginViewController {
    NSURLConnection *connection;
    NSMutableData *responseData;
}

@dynamic view;

- (void)loadView {
    self.view = [InstagramView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    responseData = [NSMutableData new];
    self.view.webview.delegate = self;
    [self authorize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)authorize {
    NSString *scopeStr = @"scope=likes+comments+relationships";
    
    NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/authorize/?client_id=%@&display=touch&%@&redirect_uri=http://www.obodev.com&response_type=code", INSTAGRAM_CLIENT_ID, scopeStr];
    
    [self.view.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.view.webview stopLoading];
    
    if([error code] == -1009) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot open the page because it is not connected to the Internet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *responseURL = [request.URL absoluteString];
    
    NSString *urlCallbackPrefix = [NSString stringWithFormat:@"%@/?code=", INSTAGRAM_CALLBACK_BASE];
    
    //We received the code, now request the auth token from Instagram.
    if([responseURL hasPrefix:urlCallbackPrefix]) {
        NSString *authToken          = [responseURL substringFromIndex:[urlCallbackPrefix length]];
        NSURL *url                   = [NSURL URLWithString:@"https://api.instagram.com/oauth/access_token"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        
        NSDictionary *paramDict = @{ @"code"          : authToken,
                                     @"redirect_uri"  : INSTAGRAM_CALLBACK_BASE,
                                     @"grant_type"    : @"authorization_code",
                                     @"client_id"     : INSTAGRAM_CLIENT_ID,
                                     @"client_secret" : INSTAGRAM_CLIENT_SECRET
                                     };

        
        NSString *paramString = [paramDict urlEncodedString];
        
        NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
        
        [request setHTTPMethod:@"POST"];
        [request addValue:[NSString stringWithFormat:@"application/x-www-form-urlencoded; charset=%@",charset] forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
        
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

        [connection start];
        
        return NO;
    }
    
    return YES;
}

#pragma Mark NSURLConnection delegates

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return cachedResponse;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)_data {
    [responseData appendData:_data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    responseData = nil;
    NSLog(@"connection didFailWithError:%@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *jsonError = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
    
    if (jsonData && !jsonError) {
        [self.authDelegate didAuthWithParams:jsonData];
        return;
    }
    
    [self.authDelegate didAuthWithParams:nil];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse {
    return request;
}

@end

//
//  ImageCell.h
//  InstagramTestApp
//
//  Created by Fantom on 8/16/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "HorizontalTableViewCell.h"

@interface ImageCell : HorizontalTableViewCell

@property (nonatomic) UIImageView *imgView;
@property (nonatomic) UILabel *commentsCounterLabel;

- (void)resetAllData;

@end

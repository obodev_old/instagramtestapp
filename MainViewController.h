//
//  MainViewController.h
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HorizontalTableViewController.h"

@interface MainViewController : HorizontalTableViewController <HorizontalTableViewDelegate>

@end

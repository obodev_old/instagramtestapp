//
//  LoginViewController.m
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "LoginViewController.h"
#import "MainViewController.h"
#import "InstagramManager.h"


@implementation LoginViewController

@dynamic view;

- (void)loadView {
    self.view = [LoginView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view.loginButton addTarget:self action:@selector(loginWithInstagram) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loginWithInstagram {
    [[InstagramManager sharedInstance] loginWithCompletion:^(NSString *result){
        if ([result isEqualToString:@"OK"]) {
            [self.navigationController pushViewController:[MainViewController new] animated:YES];
        }
    }];
}

@end

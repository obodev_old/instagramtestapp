//
//  ImageCell.m
//  InstagramTestApp
//
//  Created by Fantom on 8/16/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "ImageCell.h"

@implementation ImageCell

- (instancetype)init {
    self = [super init];
    if (self) {
        _imgView                 = [UIImageView new];
        _imgView.contentMode     = UIViewContentModeScaleAspectFit;
        [self addSubview:_imgView];
        
        _commentsCounterLabel = [UILabel new];
        _commentsCounterLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_commentsCounterLabel];
    }
    
    return self;
}

- (void)resetAllData {
    _imgView.image = nil;
    _commentsCounterLabel.text = @"";
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    static const int imageSize = 300;
    
    _imgView.frame = CGRectMake(self.bounds.size.width/2 - imageSize/2,
                                self.bounds.size.height/2 - imageSize/2,
                                imageSize,
                                imageSize);
    _commentsCounterLabel.frame = CGRectMake(_imgView.frame.origin.x,
                                             CGRectGetMaxY(_imgView.frame) + 40,
                                             _imgView.frame.size.width,
                                             50);
}

@end

//
//  InstagramView.h
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstagramView : UIView

@property (nonatomic) UIWebView *webview;

@end

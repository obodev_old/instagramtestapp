//
//  HorizontalTableView.m
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "HorizontalTableView.h"

@implementation HorizontalTableView {
    NSInteger totalRows;
    NSInteger maxVisibleRows;
    
    NSMutableArray *widthsForRows;
    NSMutableArray *reusableCells;
    
    NSMutableDictionary *allCellTypes;
    NSMutableDictionary *allReusableCells;
    
    BOOL didScrollTableView;
}

@synthesize tableViewDelegate = _tableViewDelegate;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.bounces         = NO;
        self.backgroundColor = [UIColor whiteColor];
        
        allCellTypes     = [NSMutableDictionary new];
        allReusableCells = [NSMutableDictionary new];
        
        didScrollTableView = NO;
    }
    
    return self;
}

#pragma mark Prepare tableView

- (void)registerClass:(Class)cellClass forCellReuseIdentifier:(NSString *)identifier {
    NSString *className = NSStringFromClass(cellClass);

    [allCellTypes setObject:className forKey:identifier];
}

- (void)prepareTableView {
    widthsForRows = [NSMutableArray new];
    
    totalRows = [_tableViewDelegate numberOfCells];
    
    for (int indexPath = 0; indexPath < totalRows; indexPath++) {
        [widthsForRows addObject:[NSNumber numberWithInteger:[_tableViewDelegate widthForCellAtIndexPath:indexPath]]];
    }
    
    [self createReusableCells];
}

- (void)createReusableCells {
    NSNumber *totalWidth        = [widthsForRows valueForKeyPath:@"@sum.self"];
    NSInteger totalVisibleCells = [self getMaxCellsOnScreen];
    
    if (!totalVisibleCells) {
        return;
    }
    
    [self initReusableCellsDictWithLimit:totalVisibleCells + 2];
    
    reusableCells = [NSMutableArray new];

    for (NSInteger indexPath = 0; indexPath < totalVisibleCells; indexPath++) {
        HorizontalTableViewCell *targetView = [self.tableViewDelegate tableView:self cellForRowAtIndexPath:indexPath];
        targetView.indexInTableView         = indexPath;
        
        [reusableCells addObject:targetView];
    }

    if (totalVisibleCells < totalRows) {
        [self addExtraCellInTableViewFromIndex:totalVisibleCells];
    }

    self.contentSize = CGSizeMake([totalWidth integerValue],
                                  self.bounds.size.height);
}

- (void)addExtraCellInTableViewFromIndex:(NSInteger)index {
    NSInteger extraCells = 2;
    
    for (int i = 0; i < extraCells && index < totalRows; i ++) {
        HorizontalTableViewCell *extraView = [self.tableViewDelegate tableView:self cellForRowAtIndexPath:index];
        extraView.indexInTableView         = index;
        
        [reusableCells addObject:extraView];
        index += 1;
    }
}

#pragma mark Reusing view's on scroll

- (void)didScrollToTheLeft {
    didScrollTableView = YES;

    HorizontalTableViewCell *firstView = reusableCells[0];
    HorizontalTableViewCell *lastView  = [reusableCells lastObject];
    
    if (!CGRectIntersectsRect(self.bounds, firstView.frame) && lastView.indexInTableView < totalRows - 1) {
        NSInteger nextIndex = lastView.indexInTableView + 1;
        NSInteger nextWidth = [widthsForRows[nextIndex] integerValue];
        
        //removing non visible view back to array
        firstView.frame = CGRectMake(0, 0, 0, 0);
        [self returnReusableCellToArray:firstView];
        [reusableCells removeObject:firstView];
        
        //getting new reusable cell for specifique identifier
        HorizontalTableViewCell *targetView = [self.tableViewDelegate tableView:self cellForRowAtIndexPath:nextIndex];
        targetView.frame            = CGRectMake(CGRectGetMaxX(lastView.frame), 0, nextWidth, self.bounds.size.height);
        targetView.indexInTableView = nextIndex;
        [reusableCells addObject:targetView];
    }
}

- (void)didScrollToTheRight {
    didScrollTableView = YES;

    HorizontalTableViewCell *firstView = reusableCells[0];
    HorizontalTableViewCell *lastView  = [reusableCells lastObject];
    
    if (!CGRectIntersectsRect(self.bounds, lastView.frame) && firstView.indexInTableView > 0) {
        NSInteger prevIndex = firstView.indexInTableView - 1;
        NSInteger prevWidth = [widthsForRows[prevIndex] integerValue];
        
        //removing non visible view back to array
        lastView.frame = CGRectMake(0, 0, 0, 0);
        [self returnReusableCellToArray:lastView];
        [reusableCells removeObject:lastView];
        
        //getting new reusable cell for specifique identifier
        HorizontalTableViewCell *targetView = [self.tableViewDelegate tableView:self cellForRowAtIndexPath:prevIndex];
        targetView.frame = CGRectMake(firstView.frame.origin.x - prevWidth, 0, prevWidth, self.bounds.size.height);
        targetView.indexInTableView = prevIndex;
        [reusableCells insertObject:targetView atIndex:0];
    }
}

#pragma mark Cells methods

- (NSInteger)getMaxCellsOnScreen {
    NSSortDescriptor *descriptor   = [[NSSortDescriptor alloc] initWithKey:@"" ascending:YES];
    NSArray          *sortedWidths = [widthsForRows sortedArrayUsingDescriptors:@[descriptor]];
    
    NSInteger widthCounter      = 0;
    NSInteger totalVisibleCells = 0;
    NSInteger tableViewWidth    = self.bounds.size.width;
    
    reusableCells = [NSMutableArray new];
    
    for (int index = 0; index < [sortedWidths count]; index++) {
        if (widthCounter < tableViewWidth) {
            widthCounter += [sortedWidths[index] integerValue];
        } else {
            totalVisibleCells = index + 1;
            index = (int)[sortedWidths count];
        }
    }
    
    return totalVisibleCells;
}

- (HorizontalTableViewCell *)cellForRowAtIndexPath:(NSInteger)indexPath {
    for (HorizontalTableViewCell *cell in reusableCells) {
        if (cell.indexInTableView == indexPath) {
            return cell;
        }
    }
    
    return nil;
}

#pragma mark Dictionary with all Reusable cells

- (void)initReusableCellsDictWithLimit:(NSInteger)limit {
    NSInteger totalObjects = [allCellTypes count];
    
    if (!totalObjects) {
        [allCellTypes setObject:@"HorizontalTableViewCell" forKey:@"defaultCell"];
        totalObjects = 1;
    }
    
    for (int i = 0; i < totalObjects; i++) {
        NSString *className  = [allCellTypes allValues][i];
        NSString *identifier = [allCellTypes allKeys][i];
        
        NSMutableArray *arrayOfCells = [NSMutableArray new];
        
        for (int i = 0; i < limit; i++) {
            id cellType = [[NSClassFromString(className) alloc] init];
            ((HorizontalTableViewCell *)cellType).reuseIdentifier = identifier;
            ((HorizontalTableViewCell *)cellType).frame           = CGRectMake(0, 0, 0, 0);
            ((HorizontalTableViewCell *)cellType).hidden          = YES;
            
            [self addSubview:cellType];
            [arrayOfCells addObject:cellType];
        }
        
        [allReusableCells setObject:arrayOfCells forKey:identifier];
    }
}

- (HorizontalTableViewCell *)dequeueReusableCellWithIdentifier:(NSString *)identifier {
    NSMutableArray *targetArrayOfCells = [allReusableCells objectForKey:identifier];
    HorizontalTableViewCell *cell      = targetArrayOfCells[0];
    cell.hidden                        = NO;
    
    [targetArrayOfCells removeObject:cell];
    
    return cell;
}

- (void)returnReusableCellToArray:(HorizontalTableViewCell *)cell {
    if ([cell respondsToSelector:@selector(resetAllData)]) {
        [cell resetAllData];
    }
    
    cell.hidden = YES;
    NSMutableArray *arrayOfCells = [allReusableCells objectForKey:cell.reuseIdentifier];
    [arrayOfCells addObject:cell];
}

- (void)reloadData {
    [self prepareTableView];
}

- (NSMutableArray *)indexPathsForVisibleRows {
    NSMutableArray *allVisibleCellIds = [NSMutableArray new];
    
    for (HorizontalTableViewCell *cell in reusableCells) {
        [allVisibleCellIds addObject:@(cell.indexInTableView)];
    }
    
    return allVisibleCellIds;
}

#pragma mark Layout subviews

- (void)layoutSubviews {
    if (didScrollTableView) {
        return;
    }
    
    int defaultX   = 0;
    int defaultY   = 0;
    
    for (int i = 0; i < [reusableCells count]; i++) {
        UIView *view = reusableCells[i];
        
        view.frame = CGRectMake(defaultX,
                                defaultY,
                                [widthsForRows[i] intValue],
                                self.bounds.size.height);
        defaultX += [widthsForRows[i] intValue];
    }
}

@end

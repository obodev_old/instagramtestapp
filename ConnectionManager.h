//
//  ConnectionManager.h
//  Limeet
//
//  Created by Fantom on 7/26/15.
//  Copyright (c) 2015 Obodev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject <NSURLConnectionDelegate>

- (void)loadDataFromURL:(NSURL *)url withCompletion:(void(^)(NSData *result))completionHandler;

@end

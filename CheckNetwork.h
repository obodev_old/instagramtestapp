//
//  CheckNetwork.h
//  MyTennisOpen
//
//  Created by Brice Cerival on 26/04/13.
//  Copyright (c) 2013 genicorpinnovation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckNetwork : NSObject

+ (BOOL)connectedToNetwork;
+ (BOOL)checkServerStatusForURLString:(NSString *)urlStr;

@end

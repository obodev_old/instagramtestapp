//
//  APIHelper.m
//  Limeet
//
//  Created by Fantom on 7/14/15.
//  Copyright (c) 2015 Obodev. All rights reserved.
//

#import "APIHelper.h"
#import "ConnectionManager.h"
#import "InstagramManager.h"

#include <netinet/in.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@implementation APIHelper {
    NSMutableArray *allMediaData;
    NSMutableArray *allConnectionManagers;
}

static NSString *const baseURL = @"https://api.instagram.com/v1";

- (instancetype)init {
    self = [super init];
    if (self) {
        allConnectionManagers = [NSMutableArray new];
    }
    
    return self;
}

+ (APIHelper *)sharedHelper {
    static dispatch_once_t once;
    static APIHelper *sharedInstance;
    
    dispatch_once(&once, ^ {
        sharedInstance = [self new];
    });
    
    return sharedInstance;
}

#pragma mark loading all media recursively

- (void)loadRecentMediaWithCompletion:(void(^)(NSArray *))completion {
    if (![self connectedToNetwork]) {
        [self showAlert];

        completion(nil);
        return;
    }
    
    allMediaData = [NSMutableArray new];
    
    NSString *URL = [NSString stringWithFormat:@"%@/users/%@/media/recent/?access_token=%@",
                        baseURL,
                        [[InstagramManager sharedInstance] userID],
                        [[InstagramManager sharedInstance] uniqueToken]
                    ];
    
    [self loadDataFromURL:URL withCompletion:completion];
}

- (void)loadDataFromURL:(NSString *)URL withCompletion:(void (^)(NSArray *))completionHandler {
    if (![self connectedToNetwork]) {
        [self showAlert];
        
        completionHandler([allMediaData copy]);
        allMediaData = nil;
        return;
    }
    
    ConnectionManager *connectionManager = [ConnectionManager new];
    __weak ConnectionManager *weakConnectionManager = connectionManager;
    
    [connectionManager loadDataFromURL:[NSURL URLWithString:URL] withCompletion:^(NSData *responseData) {
        if (responseData != nil) {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseData options:nil error:nil];
            NSArray *data     = result[@"data"];
            NSString *nextUrl = result[@"pagination"][@"next_url"];
            
            if ([data count]) {
                [allMediaData addObjectsFromArray:data];
            }
            if (nextUrl) {
                [self loadDataFromURL:nextUrl withCompletion:completionHandler];
            } else {
                completionHandler([allMediaData copy]);
                allMediaData = nil;
            }
            
        } else {
            completionHandler([allMediaData copy]);
            allMediaData = nil;
        }
        
        [allConnectionManagers removeObject:weakConnectionManager];
    }];
    
    [allConnectionManagers addObject:connectionManager];
}

#pragma mark No Internet Alert

- (void)showAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                    message:@"Please check Internet connection"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil,
                          nil];
    [alert show];
}

#pragma mark Check Connection

- (BOOL)connectedToNetwork {
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        NSLog(@"Error. Could not recover network reachability flags");
        return 0;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    //below suggested by Ariel
    BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.google.com/"]; //comment by friendlydeveloper: maybe use www.google.com
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    //NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //suggested by Ariel
    NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil]; //modified by friendlydeveloper
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}

#pragma mark Other methods

@end

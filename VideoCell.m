//
//  CustomCell1.m
//  InstagramTestApp
//
//  Created by Fantom on 8/16/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "VideoCell.h"

@implementation VideoCell {
    MPMoviePlayerController *videoPlayer;
    BOOL buttonIsSelected;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        videoPlayer                = [MPMoviePlayerController new];
        videoPlayer.controlStyle   = MPMovieControlStyleNone;

        videoPlayer.shouldAutoplay = NO;
        videoPlayer.scalingMode    = MPMovieScalingModeAspectFill;
        
        [videoPlayer setFullscreen:NO animated:NO];
        [self addSubview:videoPlayer.view];
        
        _commentsCounterLabel = [UILabel new];
        _commentsCounterLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_commentsCounterLabel];
        
        _playButton = [UIButton new];
        _playButton.backgroundColor = [UIColor redColor];
        [_playButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [_playButton setTitle:@"Play" forState:UIControlStateNormal];
        [_playButton setTitle:@"Stop" forState:UIControlStateSelected];
        [_playButton addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_playButton];
    }
    
    return self;
}

- (void)prepareToPlay {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4", _videoName]];
    
    videoPlayer.contentURL = [NSURL fileURLWithPath:appFile];
    [videoPlayer prepareToPlay];
}

- (void)playVideo {
//    if (![videoPlayer readyForDisplay]) {
//        return;
//    }
    if (!buttonIsSelected) {
        [videoPlayer play];
        [_playButton setSelected:YES];
        buttonIsSelected = !buttonIsSelected;
        return;
    }
    
    [self stopVideo];
}

- (void)stopVideo {
    [videoPlayer pause];
    [_playButton setSelected:NO];
    buttonIsSelected = !buttonIsSelected;
}

- (void)resetAllData {
    buttonIsSelected = NO;
    [_playButton setSelected:NO];
    [videoPlayer stop];
    [videoPlayer setCurrentPlaybackTime:videoPlayer.endPlaybackTime];
    videoPlayer.contentURL = nil;
    _commentsCounterLabel.text = @"";
}


- (void)layoutSubviews {
    [super layoutSubviews];

    static const int videoSize = 300;
    static const int btnSize = 100;

    
   videoPlayer.view.frame = CGRectMake(self.bounds.size.width/2 - videoSize/2,
                                self.bounds.size.height/2 - videoSize/2,
                                videoSize,
                                videoSize);
    
    _commentsCounterLabel.frame = CGRectMake(videoPlayer.view.frame.origin.x,
                                             CGRectGetMaxY(videoPlayer.view.frame) + 40,
                                             videoPlayer.view.frame.size.width,
                                             50);
    _playButton.frame = CGRectMake(self.bounds.size.width/2 - btnSize/2,
                                   self.bounds.size.height - btnSize/2 - 15,
                                   btnSize,
                                   btnSize/2);
}

@end

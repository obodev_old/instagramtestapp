//
//  HorizontalTableViewCell.m
//  InstagramTestApp
//
//  Created by Fantom on 8/15/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "HorizontalTableViewCell.h"

@implementation HorizontalTableViewCell

- (instancetype)init {
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

- (void)resetAllData {
    
}

@end

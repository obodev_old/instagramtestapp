//
//  CustomCell1.h
//  InstagramTestApp
//
//  Created by Fantom on 8/16/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import "HorizontalTableViewCell.h"
@import MediaPlayer;

@interface VideoCell : HorizontalTableViewCell

@property (nonatomic) UILabel *commentsCounterLabel;
@property (nonatomic) NSString *videoName;
@property (nonatomic) UIButton *playButton;

- (void)prepareToPlay;
- (void)resetAllData;

@end

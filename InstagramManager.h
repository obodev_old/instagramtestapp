//
//  InstagramManager.h
//  InstagramTestApp
//
//  Created by Fantom on 8/17/15.
//  Copyright (c) 2015 obodev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstargamLoginViewController.h"

@interface InstagramManager : NSObject <InstagramAuthDelegate>

+ (InstagramManager *)sharedInstance;

@property (nonatomic) NSString *userID;
@property (nonatomic) NSString *uniqueToken;
@property (nonatomic) NSMutableArray *mediaData;

- (void)loginWithCompletion:(void(^)(NSString *result))completion;
- (void)loadAllMediaWithCompletion:(void(^)(NSArray *result))completion;

@end
